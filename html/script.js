const loginFormDiv = document.getElementById("login-form");
const messageContainerDiv = document.getElementById("message-container");
const messageListDiv = document.getElementById("message-list");

const loginButton = document.getElementById("login-button");
const loginInput = document.getElementById("new-user");

const messageButton = document.getElementById("message-button");
const messageInput = document.getElementById("new-message");

const localUser = localStorage.getItem("user");
let user = localUser ? JSON.parse(localUser) : null;

let ws;

let messages = [];

console.log("Benutzer gefunden?", user);

refreshContainerVisibility = () => {
  if (!user) {
    loginFormDiv.style.display = "block";
    messageContainerDiv.style.display = "none";
  } else {
    loginFormDiv.style.display = "none";
    messageContainerDiv.style.display = "block";
  }
};

refreshMessageContainer = () => {
  if (!messages.length) {
    messageListDiv.innerText = "Keine Nachrichten gefunden.";
    return;
  }

const messagesClone = [...messages];

  const divContent = messagesClone
  .reverse()
    .map((k) => {
      return `<div class="message${k.user.id === user.id ? ' self' : ''}"><div class="name">${k.user.name}</div><div class="timestamp">${new Date(k.timestamp).getHours().toString().padStart(2, "0")}:${new Date(k.timestamp).getMinutes().toString().padStart(2, "0")}</div><div class="message-content">${k.message}</div></div>`;
    })
    .join(" ");

  messageListDiv.innerHTML = divContent;
};

getAllMessages = () => {
  axios
    .get("http://localhost:5000/api/chat/message?userid=" + user.id)
    .then((mes) => {
      messages = mes.data;
      console.log("Nachrichten abgerufen: ", messages);
      refreshMessageContainer();
    });
};

loginButton.addEventListener("click", () => {
  const userName = loginInput.value;

  if (!userName) return;

  axios
    .post("http://localhost:5000/api/user/register", {
      name: userName,
    })
    .then((newUser) => {
      console.log("Benutzer angemeldet: ", newUser.data);
      user = newUser.data;
      localStorage.setItem("user", JSON.stringify(user));
      refreshContainerVisibility();
      getAllMessages();
      initWebsocket();
    });
});

sendMessage = () => {
    const message = messageInput.value;
  
    if (!message) return;
  
    axios
      .post("http://localhost:5000/api/chat/message?userid=" + user.id, {
        message: message,
      })
      .then((newMessage) => {
        console.log("Nachricht gesendet: ", newMessage.data);
        messages.push(newMessage.data);
        refreshMessageContainer();
      });
}


messageButton.addEventListener("click", () => {
    sendMessage();
  });

  messageInput.addEventListener("keyup", (event) => {
      event.preventDefault();
    if (event.key === 'Enter') {
        sendMessage();
    }
  })

refreshContainerVisibility();


initWebsocket = () => {
   ws = new WebSocket("ws://localhost:5001?userid=" + user.id);
   /**
 * Funktion, welche beim Erfolgreichen Öffnen des Websockets abgearbeitet wird
 */
 ws.onopen = function () {
    console.log(`Erfolgreich mit WebSocket verbunden`);
  };
  
  /**
   * Funktion die bei einer neuen Nachricht ausgeführt wird.
   *
   * @param messageEvent Event mit neuer Nachricht.
   */
  ws.onmessage = function (messageEvent) {
    const data = JSON.parse(messageEvent.data);
    console.log(`Neue Nachricht empfangen`, data);

    if(data.action && data.action === "new-message") {
        messages.push(data.message);
        refreshMessageContainer();
    }
  };

}


if (user) {
    getAllMessages();
    initWebsocket();
  }